package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestCheckHealth(t *testing.T) {
	r := gin.Default()
	r.GET("/api/livez", checkHealth)

	req := httptest.NewRequest("GET", "/api/livez", nil)
	res := httptest.NewRecorder()
	r.ServeHTTP(res, req)

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	b, _ := io.ReadAll(res.Body)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, "ok", string(b))
}
