# Skaffold

## Getting started

1. get k8s context

- az login && az aks get-credentials --resource-group devops-nonprod-aks-rg --name devops-nonprod-aks

2. login docker registry

- az acr login --name xxx --expose-token --output tsv --query accessToken | docker login  --username 00000000-0000-0000-0000-000000000000 --password-stdin xxx.azurecr.io

3. install skaffold

- brew install skaffold

4. start skaffold

- skaffold run --default-repo idtechacr.azurecr.io
- skaffold dev --default-repo idtechacr.azurecr.io --port-forward
- skaffold build --default-repo idtechacr.azurecr.io -q | skaffold deploy -a -
