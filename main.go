package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	dbConnStr := os.Getenv("DB_CONN_STR")
	log.Println("db connection string:", dbConnStr)

	r := gin.Default()
	// static file
	httpFS := gin.Dir("./public", false)
	fileServer := http.FileServer(httpFS)
	r.NoRoute(gin.WrapH(fileServer))

	api := r.Group("/api")
	api.GET("/livez", checkHealth)
	r.Run(":3000")
}

func checkHealth(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}
